function situacaoAnimal(regiao, animal) {

    if (regiao.toLowerCase() === 'norte') {

        switch (animal.toLowerCase()) {
            case "condor negro":
                return "Ameaçado de extinção";
                break;
            case "sapo cururu":
                return "Abundante";
                break;
            default:
                return "Eitcha lele, esse animal não faz parte dessa parte aqui!";
                break;
        }
    } else if (regiao.toLowerCase() === 'sul') {

        return animal.toLowerCase() === 'mamute' ? 'Extinto' : "Ahhhhh, 404 animal not achado";
    } else {
        return 'Errrrroooooou, existe essa região não, pelo menos não aqui :D';
    }

}