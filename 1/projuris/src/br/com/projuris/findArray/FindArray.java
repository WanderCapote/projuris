package br.com.projuris.findArray;

public interface FindArray {
	
	int findArray(int[] array, int[] subArray);

}
