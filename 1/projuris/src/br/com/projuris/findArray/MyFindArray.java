package br.com.projuris.findArray;

import java.util.Arrays;
import java.util.stream.Collectors;

public class MyFindArray implements FindArray{

	public static void main(String[] args) {
		
		MyFindArray main = new MyFindArray();

		int[] array1 = {4,9,3,7,8};
		int[] subArray1 = {3,7};
		int[] array2 = {1,3,5};
		int[] subArray2 = {1};
		int[] array3 = {7,8,9};
		int[] subArray3 = {8,9,10};
		int[] array4 = {4,9,3,7,8,3,7,1};
		int[] subArray4 = {3,7};
		int[] array5 = {4,9,3,7,8,3,7,1};
		int[] subArray5 = {0,7};

		System.out.println(main.findArray(array1, subArray1));
		System.out.println(main.findArray(array2, subArray2));
		System.out.println(main.findArray(array3, subArray3));
		System.out.println(main.findArray(array4, subArray4));
		System.out.println(main.findArray(array5, subArray5));

	}

	@Override
	public int findArray(int[] array, int[] subArray) {

		return Arrays.stream(array).boxed().collect(Collectors.toList()).lastIndexOf(subArray[0]);		

	}

}
