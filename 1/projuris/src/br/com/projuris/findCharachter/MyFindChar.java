package br.com.projuris.findCharachter;

public class MyFindChar implements FindCharachter {

	public static void main(String[] args) {

		MyFindChar main = new MyFindChar();

		System.out.println(main.findChar("stress"));
		System.out.println(main.findChar("reembolsar"));
		System.out.println(main.findChar("atitude"));
		System.out.println(main.findChar("assinatura"));
		System.out.println(main.findChar("ssssss"));

	}

	@Override
	public char findChar(String word) {

		for (int i = 0; i < word.length(); i++) {

			char letter = word.charAt(i);
			if (word.chars().filter(letters -> letters == letter).count() == 1) {

				return letter;
			}

		}

		return 0;

	}

}
