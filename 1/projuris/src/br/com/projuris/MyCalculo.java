package br.com.projuris;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class MyCalculo implements Calculo {

	public static void main(String[] args) {

		Funcionario funcionario1 = new Funcionario("Assistente", "Administrativo", new BigDecimal(1000.0));
		Funcionario funcionario2 = new Funcionario("Gerente", "Administrativo", new BigDecimal(7000.70));
		Funcionario funcionario3 = new Funcionario("Diretor", "Administrativo", new BigDecimal(10000.45));
		Funcionario funcionario4 = new Funcionario("Assistente", "Financeiro", new BigDecimal(1300.9));
		Funcionario funcionario5 = new Funcionario("Gerente", "Financeiro", new BigDecimal(7500));
		Funcionario funcionario6 = new Funcionario("Diretor", "Financeiro", new BigDecimal(11000.0));
		Funcionario funcionario7 = new Funcionario("Estagi�rio", "Jur�dico", new BigDecimal(700.4));
		Funcionario funcionario8 = new Funcionario("Assistente", "Jur�dico", new BigDecimal(1800.90));
		Funcionario funcionario9 = new Funcionario("Gerente", "Jur�dico", new BigDecimal(9500.50));
		Funcionario funcionario10 = new Funcionario("Diretor", "Jur�dico", new BigDecimal(13000.0));
		List<Funcionario> listaFuncionario = new ArrayList<>();
		listaFuncionario.add(funcionario1);
		listaFuncionario.add(funcionario2);
		listaFuncionario.add(funcionario3);
		listaFuncionario.add(funcionario4);
		listaFuncionario.add(funcionario5);
		listaFuncionario.add(funcionario6);
		listaFuncionario.add(funcionario7);
		listaFuncionario.add(funcionario8);
		listaFuncionario.add(funcionario9);
		listaFuncionario.add(funcionario10);

		MyCalculo myCalculo = new MyCalculo();

		System.out.println("Custo por Cargo");
		myCalculo.custoPorCargo(listaFuncionario).forEach(cargo -> {
			System.out.println("Cargo: " + cargo.getCargo());
			System.out.println("Custo: " + cargo.getCusto());
			System.out.println("-----------------");
		});
		System.out.println("Calculo de Custo por Cargo finalizado");
		System.out.println("-----------------");

		System.out.println("Custo por Departamento");
		myCalculo.custoPorDepartamento(listaFuncionario).forEach(cargo -> {
			System.out.println("Departamento: " + cargo.getDepartamento());
			System.out.println("Custo: " + cargo.getCusto());
			System.out.println("-----------------");
		});
		System.out.println("Calculo de Custo por Departamento finalizado");

	}

	@Override
	public List<CustoCargo> custoPorCargo(List<Funcionario> funcionarios) {
		List<CustoCargo> lista = new ArrayList<>();
		funcionarios.forEach((f) -> {

			CustoCargo ccc = lista.stream().filter(cc -> cc.getCargo().equals(f.getCargo())).findFirst().orElse(null);

			if (ccc != null) {
				ccc.setCusto(ccc.getCusto().add(f.getSalario()));
			} else {
				CustoCargo cc = new CustoCargo();
				cc.setCargo(f.getCargo());
				cc.setCusto(f.getSalario());
				lista.add(cc);
			}

		});

		return lista;
	}

	@Override
	public List<CustoDepartamento> custoPorDepartamento(List<Funcionario> funcionarios) {
		List<CustoDepartamento> lista = new ArrayList<>();
		funcionarios.forEach((f) -> {

			CustoDepartamento cdd = lista.stream().filter(cd -> cd.getDepartamento().equals(f.getDepartamento()))
					.findFirst().orElse(null);

			if (cdd != null) {
				cdd.setCusto(cdd.getCusto().add(f.getSalario()));
			} else {
				CustoDepartamento cd = new CustoDepartamento();
				cd.setDepartamento(f.getDepartamento());
				cd.setCusto(f.getSalario());
				lista.add(cd);
			}
		});

		return lista;
	}

}
